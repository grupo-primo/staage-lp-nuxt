const _env = {
  BACKSTAAGE: [
    {
      id: '71ee7t7e-cb49-4d55-a67f-a418d0d1bb5b',
      courseGroup: 'Backstaage',
      courseTitle: 'O Recomeço',
      courseCenter: 'Thiago Nigro',
      poster: 'https://content.staage.com/_c/71ee7t7e-cb49-4d55-a67f-a418d0d1bb5b/_p2.png',
      courseDescription: 'Conheça os bastidores do Grupo Primo e vivencie um lançamento digital na prática',
      slug: 'recomeco-thiago-nigro'
    },
    {
      id: '53cdfb98-a909-4a05-89f8-f1962f58b816',
      courseGroup: 'Backstaage',
      courseTitle: 'O Sobrevivente',
      courseCenter: 'Bruno Perini',
      poster: 'https://content.staage.com/_c/53cdfb98-a909-4a05-89f8-f1962f58b816/_p.png',
      courseDescription: 'Acompanhe como se faz um lançamento milionário',
      slug: 'o-sobrevivente-bruno-perini'
    },
    {
      id: 'da5b4536-5fb9-4b8d-ba1a-b97f63e15af7',
      courseGroup: 'Backstaage',
      courseTitle: 'Investidor em 72 horas',
      courseCenter: 'Breno Perrucho',
      poster: 'https://content.staage.com/_c/da5b4536-5fb9-4b8d-ba1a-b97f63e15af7/_p2.png',
      courseDescription: 'A construção de um produto de recorrência',
      slug: 'investidor-breno-perrucho-jovens'
    },
    {
      id: '7ee3495d-9974-4049-9ceb-5010a7e88af3',
      courseGroup: 'Backstaage',
      courseTitle: 'Maestria Profissional',
      courseCenter: 'Joel Jota',
      poster: 'https://content.staage.com/_c/7ee3495d-9974-4049-9ceb-5010a7e88af3/_p.png',
      courseDescription: 'Os desafios de um lançamento de 5 dias no Instagram',
      slug: 'joel-jota-maestrial-professional'
    },
    {
      id: '1b3cc3f5-4afa-4fa2-b9e6-c13a385f54ad',
      courseGroup: 'Backstaage',
      courseTitle: 'O Legado',
      courseCenter: 'Thiago Nigro',
      poster: 'https://content.staage.com/_c/1b3cc3f5-4afa-4fa2-b9e6-c13a385f54ad/_p2.png',
      courseDescription: 'Dia a dia para fazer múltiplos 7 digítos de faturamento',
      slug: 'thiago-nigro-primo-rico-lancamento-legado'
    },
    {
      id: 'ded8e72e-0220-4491-b939-fbcd86f5badd',
      courseGroup: 'Backstaage',
      courseTitle: 'Norton Mello',
      courseCenter: 'Norton Mello',
      poster: 'https://content.staage.com/_c/ded8e72e-0220-4491-b939-fbcd86f5badd/_p2.png',
      courseDescription: 'A construção de um produto de recorrência',
      slug: 'treinos-com-norton-mello'
    },
    {
      id: '2ce8a250-510f-420e-a274-4a5296d48377',
      courseGroup: 'Backstaage',
      courseTitle: 'Bastidores de uma estratégia inédita',
      courseCenter: 'Ju Fraccaroli',
      poster: 'https://content.staage.com/_c/2ce8a250-510f-420e-a274-4a5296d48377/_p2.png',
      courseDescription: 'Saindo dos bastidores e lançando o próprio produto',
      slug: 'metodo-ju-fraccaroli'
    }
  ],
  STAAGECLASS: [
    {
      id: 'fea4c510-4927-48af-9eb5-0128b3d94418',
      courseGroup: 'Staageclass',
      courseTitle: 'Como tirar seus projetos do papel',
      courseCenter: 'Joel Jota',
      poster: 'https://content.staage.com/_c/fea4c510-4927-48af-9eb5-0128b3d94418/_p.png',
      courseDescription: 'Joel Jota',
      slug: 'joel-jota-como-tirar-ideias-papel'
    },
    {
      id: '53ddde71-eb0f-4f03-bd68-324b51867b00',
      courseGroup: 'Staageclass',
      courseTitle: '#NaPrática: fotos profissionais para instagram',
      courseCenter: 'Rodolfo Corradin',
      poster: 'https://content.staage.com/_c/53ddde71-eb0f-4f03-bd68-324b51867b00/_p.png',
      courseDescription: 'Rodolfo Corradin',
      slug: 'rodolfo-corradin-fotos-profissionais-instagram'
    },
    {
      id: '16fe455b-08c1-49df-9898-75a18968acce',
      courseGroup: 'Staageclass',
      courseTitle: 'Marketing 1.0 ao 5.0',
      courseCenter: 'Philip Kotler',
      poster: 'https://content.staage.com/_c/16fe455b-08c1-49df-9898-75a18968acce/_p2.png',
      courseDescription: 'Philip Kotler',
      slug: 'philip-kotler-ensina-marketing'
    },
    {
      id: '8f4bcfef-ea2f-4585-825c-daf599bdfb3e',
      courseGroup: 'Staageclass',
      courseTitle: 'Do Mil ao Milhão com Marketing Digital',
      courseCenter: 'Thiago Nigro',
      poster: 'https://content.staage.com/_c/8f4bcfef-ea2f-4585-825c-daf599bdfb3e/_p.png',
      courseDescription: 'Thiago Nigro',
      slug: 'mil-ao-milhao-primo-day'
    },
    {
      id: '02fa790f-bf46-4372-bd4c-5eb46c62dd54',
      courseGroup: 'Staageclass',
      courseTitle: '#NaPrática: como criar um e-commerce',
      courseCenter: 'Renata Estevo',
      poster: 'https://content.staage.com/_c/02fa790f-bf46-4372-bd4c-5eb46c62dd54/_p.png',
      courseDescription: 'Renata Estevo',
      slug: 'renata-estevo-ecommerce'
    },
    {
      id: '58272fd6-aed5-4212-a70c-8e22cfb08499',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer stories que engajam',
      courseCenter: 'Malu Perini',
      poster: 'https://content.staage.com/_c/58272fd6-aed5-4212-a70c-8e22cfb08499/_p.png',
      courseDescription: 'Malu Perini',
      slug: 'como-fazer-stories-que-engajam'
    },
    {
      id: '8250426c-40c1-4e35-869c-5f5b44b3aed2',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer reels virais',
      courseCenter: 'Isadora Duarte',
      poster: 'https://content.staage.com/_c/8250426c-40c1-4e35-869c-5f5b44b3aed2/_p.png',
      courseDescription: 'Isadora Duarte',
      slug: 'como-fazer-reels-virais-isadora-duarte'
    },
    {
      id: '1f421591-a23c-4a70-9d4b-562b83b75600',
      courseGroup: 'Staageclass',
      courseTitle: 'LGPD e o Marketing Digital',
      courseCenter: 'Thayssa Prado',
      poster: 'https://content.staage.com/_c/1f421591-a23c-4a70-9d4b-562b83b75600/_p.png',
      courseDescription: 'Thassya Prado',
      slug: 'thassya-prado-lgpd'
    },
    {
      id: 'ea5e456e-3582-4c18-bab4-d647b23e6a69',
      courseGroup: 'Staageclass',
      courseTitle: 'Gatilhos Mentais',
      courseCenter: 'Bruno Perini',
      poster: 'https://content.staage.com/_c/ea5e456e-3582-4c18-bab4-d647b23e6a69/_p.png',
      courseDescription: 'Bruno Perini',
      slug: 'bruno-perini-gatilhos-mentais'
    },
    {
      id: '9f14860e-ee3b-4e09-b3d0-4e0711403fe2',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer trafégo direto',
      courseCenter: 'Micha Menezes',
      poster: 'https://content.staage.com/_c/9f14860e-ee3b-4e09-b3d0-4e0711403fe2/_p2.png',
      courseDescription: 'Micha Menezes',
      slug: 'trafego-direto-micha-menezes'
    },
    {
      id: '24de6ec9-5be6-415d-9a72-5d10e4fe27eb',
      courseGroup: 'Staageclass',
      courseTitle: 'Métricas',
      courseCenter: 'Gustavo Esteves',
      poster: 'https://content.staage.com/_c/24de6ec9-5be6-415d-9a72-5d10e4fe27eb/_p2.png',
      courseDescription: 'Gustavo Esteves',
      slug: 'gustavo-esteves-metricas'
    },
    {
      id: '6aeb7161-95f9-40da-a314-1c10be18401b',
      courseGroup: 'Staageclass',
      courseTitle: 'Criando seu e-commerce do zero',
      courseCenter: 'Alejandro Vázquez',
      poster: 'https://content.staage.com/_c/8569590d-d3ae-4f7a-bfff-59600fc540f1/_p2.png',
      courseDescription: 'Alejandro Vázquez',
      slug: 'alejandro-vazquez-ecommerce'
    },
    {
      id: '6aeb7161-95f9-40da-a314-1c10be18401b',
      courseGroup: 'Staageclass',
      courseTitle: 'Fundamentos do Copywriting',
      courseCenter: 'Roberta Santos',
      poster: 'https://content.staage.com/_c/6aeb7161-95f9-40da-a314-1c10be18401b/_p.png',
      courseDescription: 'Roberta Santos',
      slug: 'roberta-santos-copywriting'
    },
    {
      id: '397b641d-0c30-4347-a634-788d1b792688',
      courseGroup: 'Staageclass',
      courseTitle: 'Como construir uma comunidade digital',
      courseCenter: 'Camila Vidal',
      poster: 'https://content.staage.com/_c/397b641d-0c30-4347-a634-788d1b792688/_p2.png',
      courseDescription: 'Camila Vidal',
      slug: 'comunidades-com-camila-vidal'
    },
    {
      id: '47ecb671-97e0-404c-99a7-7d94176a5580',
      courseGroup: 'Staageclass',
      courseTitle: 'Estratégias de Crescimento',
      courseCenter: 'Dener Lippert',
      poster: 'https://content.staage.com/_c/47ecb671-97e0-404c-99a7-7d94176a5580/_p.png',
      courseDescription: 'Dener Lippert',
      slug: 'dener-lippert-ensina-growth'
    },
    {
      id: '7a5eb05e-5c4c-476d-96f8-4113af1e9ad2',
      courseGroup: 'Staageclass',
      courseTitle: 'Vendas',
      courseCenter: 'Thiago Concer',
      poster: 'https://content.staage.com/_c/7a5eb05e-5c4c-476d-96f8-4113af1e9ad2/_p.png',
      courseDescription: 'Thiago Concer',
      slug: 'thiago-concer-ensina-vendas'
    },
    {
      id: '397652b8-ea9e-4dfd-84aa-0a7cacf16ac8',
      courseGroup: 'Staageclass',
      courseTitle: 'Como criar um podcast do zero',
      courseCenter: 'Lucas Zafra',
      poster: 'https://content.staage.com/_c/397652b8-ea9e-4dfd-84aa-0a7cacf16ac8/_p2.png',
      courseDescription: 'Lucas Zafra',
      slug: 'lucas-zafra-podcast-primocast'
    },
    {
      id: '397652b8-ea9e-4dfd-84aa-0a7cacf16ac8',
      courseGroup: 'Staageclass',
      courseTitle: 'Dominando o mercado de lançamentos',
      courseCenter: 'Ellen Salomão',
      poster: 'https://content.staage.com/_c/c895c0e8-8a6d-4fa8-9141-c09d82c75591/_p.png',
      courseDescription: 'Ellen Salomão',
      slug: 'lancamentos-digitais-com-ellen-salomao'
    },
    {
      id: '0b9fefbc-1f37-425f-a212-cb118afe8cbe',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer dinheiro no instagram',
      courseCenter: 'Duda Vieira',
      poster: 'https://content.staage.com/_c/0b9fefbc-1f37-425f-a212-cb118afe8cbe/_p.png',
      courseDescription: 'Duda Vieira',
      slug: 'duda-vieira-ensina-instagram'
    }
  ],
  COURSES: [
    {
      id: '8f4bcfef-ea2f-4585-825c-daf599bdfb3e',
      courseGroup: 'Staageclass',
      courseTitle: 'Do Mil ao Milhão com Marketing Digital',
      courseCenter: 'Thiago Nigro',
      poster: 'https://content.staage.com/_c/8f4bcfef-ea2f-4585-825c-daf599bdfb3e/_p.png',
      courseDescription: 'Thiago Nigro',
      slug: 'mil-ao-milhao-primo-day'
    },
    {
      id: '71ee7t7e-cb49-4d55-a67f-a418d0d1bb5b',
      courseGroup: 'Backstaage',
      courseTitle: 'O Recomeço',
      courseCenter: 'Thiago Nigro',
      poster: 'https://content.staage.com/_c/71ee7t7e-cb49-4d55-a67f-a418d0d1bb5b/_p2.png',
      courseDescription: 'Conheça os bastidores do Grupo Primo e vivencie um lançamento digital na prática',
      slug: 'recomeco-thiago-nigro'
    },
    {
      id: '16fe455b-08c1-49df-9898-75a18968acce',
      courseGroup: 'Staageclass',
      courseTitle: 'Marketing 1.0 ao 5.0',
      courseCenter: 'Philip Kotler',
      poster: 'https://content.staage.com/_c/16fe455b-08c1-49df-9898-75a18968acce/_p2.png',
      courseDescription: 'Philip Kotler',
      slug: 'philip-kotler-ensina-marketing'
    },
    {
      id: '53cdfb98-a909-4a05-89f8-f1962f58b816',
      courseGroup: 'Backstaage',
      courseTitle: 'O Sobrevivente',
      courseCenter: 'Bruno Perini',
      poster: 'https://content.staage.com/_c/53cdfb98-a909-4a05-89f8-f1962f58b816/_p.png',
      courseDescription: 'Acompanhe como se faz um lançamento milionário',
      slug: 'o-sobrevivente-bruno-perini'
    },
    {
      id: '02fa790f-bf46-4372-bd4c-5eb46c62dd54',
      courseGroup: 'Staageclass',
      courseTitle: '#NaPrática: como criar um e-commerce',
      courseCenter: 'Renata Estevo',
      poster: 'https://content.staage.com/_c/02fa790f-bf46-4372-bd4c-5eb46c62dd54/_p.png',
      courseDescription: 'Renata Estevo',
      slug: 'renata-estevo-ecommerce'
    },
    {
      id: 'da5b4536-5fb9-4b8d-ba1a-b97f63e15af7',
      courseGroup: 'Backstaage',
      courseTitle: 'Investidor em 72 horas',
      courseCenter: 'Breno Perrucho',
      poster: 'https://content.staage.com/_c/da5b4536-5fb9-4b8d-ba1a-b97f63e15af7/_p2.png',
      courseDescription: 'A construção de um produto de recorrência',
      slug: 'investidor-breno-perrucho-jovens'
    },
    {
      id: '8250426c-40c1-4e35-869c-5f5b44b3aed2',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer reels virais',
      courseCenter: 'Isadora Duarte',
      poster: 'https://content.staage.com/_c/8250426c-40c1-4e35-869c-5f5b44b3aed2/_p.png',
      courseDescription: 'Isadora Duarte',
      slug: 'como-fazer-reels-virais-isadora-duarte'
    },
    {
      id: '7ee3495d-9974-4049-9ceb-5010a7e88af3',
      courseGroup: 'Backstaage',
      courseTitle: 'Maestria Profissional',
      courseCenter: 'Joel Jota',
      poster: 'https://content.staage.com/_c/7ee3495d-9974-4049-9ceb-5010a7e88af3/_p.png',
      courseDescription: 'Os desafios de um lançamento de 5 dias no Instagram',
      slug: 'joel-jota-maestrial-professional'
    },
    {
      id: 'ea5e456e-3582-4c18-bab4-d647b23e6a69',
      courseGroup: 'Staageclass',
      courseTitle: 'Gatilhos Mentais',
      courseCenter: 'Bruno Perini',
      poster: 'https://content.staage.com/_c/ea5e456e-3582-4c18-bab4-d647b23e6a69/_p.png',
      courseDescription: 'Bruno Perini',
      slug: 'bruno-perini-gatilhos-mentais'
    },
    {
      id: '1b3cc3f5-4afa-4fa2-b9e6-c13a385f54ad',
      courseGroup: 'Backstaage',
      courseTitle: 'O Legado',
      courseCenter: 'Thiago Nigro',
      poster: 'https://content.staage.com/_c/1b3cc3f5-4afa-4fa2-b9e6-c13a385f54ad/_p2.png',
      courseDescription: 'Dia a dia para fazer múltiplos 7 digítos de faturamento',
      slug: 'thiago-nigro-primo-rico-lancamento-legado'
    },
    {
      id: '58272fd6-aed5-4212-a70c-8e22cfb08499',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer stories que engajam',
      courseCenter: 'Malu Perini',
      poster: 'https://content.staage.com/_c/58272fd6-aed5-4212-a70c-8e22cfb08499/_p.png',
      courseDescription: 'Malu Perini',
      slug: 'como-fazer-stories-que-engajam'
    },
    {
      id: 'ded8e72e-0220-4491-b939-fbcd86f5badd',
      courseGroup: 'Backstaage',
      courseTitle: 'Norton Mello',
      courseCenter: 'Norton Mello',
      poster: 'https://content.staage.com/_c/ded8e72e-0220-4491-b939-fbcd86f5badd/_p2.png',
      courseDescription: 'A construção de um produto de recorrência',
      slug: 'treinos-com-norton-mello'
    },
    {
      id: '24de6ec9-5be6-415d-9a72-5d10e4fe27eb',
      courseGroup: 'Staageclass',
      courseTitle: 'Métricas',
      courseCenter: 'Gustavo Esteves',
      poster: 'https://content.staage.com/_c/24de6ec9-5be6-415d-9a72-5d10e4fe27eb/_p2.png',
      courseDescription: 'Gustavo Esteves',
      slug: 'gustavo-esteves-metricas'
    },
    {
      id: '2ce8a250-510f-420e-a274-4a5296d48377',
      courseGroup: 'Backstaage',
      courseTitle: 'Bastidores de uma estratégia inédita',
      courseCenter: 'Ju Fraccaroli',
      poster: 'https://content.staage.com/_c/2ce8a250-510f-420e-a274-4a5296d48377/_p2.png',
      courseDescription: 'Saindo dos bastidores e lançando o próprio produto',
      slug: 'metodo-ju-fraccaroli'
    },
    {
      id: '9f14860e-ee3b-4e09-b3d0-4e0711403fe2',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer trafégo direto',
      courseCenter: 'Micha Menezes',
      poster: 'https://content.staage.com/_c/9f14860e-ee3b-4e09-b3d0-4e0711403fe2/_p2.png',
      courseDescription: 'Micha Menezes',
      slug: 'trafego-direto-micha-menezes'
    },
    {
      id: 'fea4c510-4927-48af-9eb5-0128b3d94418',
      courseGroup: 'Staageclass',
      courseTitle: 'Como tirar seus projetos do papel',
      courseCenter: 'Joel Jota',
      poster: 'https://content.staage.com/_c/fea4c510-4927-48af-9eb5-0128b3d94418/_p.png',
      courseDescription: 'Joel Jota',
      slug: 'joel-jota-como-tirar-ideias-papel'
    },
    {
      id: '53ddde71-eb0f-4f03-bd68-324b51867b00',
      courseGroup: 'Staageclass',
      courseTitle: '#NaPrática: fotos profissionais para instagram',
      courseCenter: 'Rodolfo Corradin',
      poster: 'https://content.staage.com/_c/53ddde71-eb0f-4f03-bd68-324b51867b00/_p.png',
      courseDescription: 'Rodolfo Corradin',
      slug: 'rodolfo-corradin-fotos-profissionais-instagram'
    },
    {
      id: '1f421591-a23c-4a70-9d4b-562b83b75600',
      courseGroup: 'Staageclass',
      courseTitle: 'LGPD e o Marketing Digital',
      courseCenter: 'Thayssa Prado',
      poster: 'https://content.staage.com/_c/1f421591-a23c-4a70-9d4b-562b83b75600/_p.png',
      courseDescription: 'Thassya Prado',
      slug: 'thassya-prado-lgpd'
    },
    {
      id: '6aeb7161-95f9-40da-a314-1c10be18401b',
      courseGroup: 'Staageclass',
      courseTitle: 'Criando seu e-commerce do zero',
      courseCenter: 'Alejandro Vázquez',
      poster: 'https://content.staage.com/_c/8569590d-d3ae-4f7a-bfff-59600fc540f1/_p2.png',
      courseDescription: 'Alejandro Vázquez',
      slug: 'alejandro-vazquez-ecommerce'
    },
    {
      id: '6aeb7161-95f9-40da-a314-1c10be18401b',
      courseGroup: 'Staageclass',
      courseTitle: 'Fundamentos do Copywriting',
      courseCenter: 'Roberta Santos',
      poster: 'https://content.staage.com/_c/6aeb7161-95f9-40da-a314-1c10be18401b/_p.png',
      courseDescription: 'Roberta Santos',
      slug: 'roberta-santos-copywriting'
    },
    {
      id: '397b641d-0c30-4347-a634-788d1b792688',
      courseGroup: 'Staageclass',
      courseTitle: 'Como construir uma comunidade digital',
      courseCenter: 'Camila Vidal',
      poster: 'https://content.staage.com/_c/397b641d-0c30-4347-a634-788d1b792688/_p2.png',
      courseDescription: 'Camila Vidal',
      slug: 'comunidades-com-camila-vidal'
    },
    {
      id: '47ecb671-97e0-404c-99a7-7d94176a5580',
      courseGroup: 'Staageclass',
      courseTitle: 'Estratégias de Crescimento',
      courseCenter: 'Dener Lippert',
      poster: 'https://content.staage.com/_c/47ecb671-97e0-404c-99a7-7d94176a5580/_p.png',
      courseDescription: 'Dener Lippert',
      slug: 'dener-lippert-ensina-growth'
    },
    {
      id: '7a5eb05e-5c4c-476d-96f8-4113af1e9ad2',
      courseGroup: 'Staageclass',
      courseTitle: 'Vendas',
      courseCenter: 'Thiago Concer',
      poster: 'https://content.staage.com/_c/7a5eb05e-5c4c-476d-96f8-4113af1e9ad2/_p.png',
      courseDescription: 'Thiago Concer',
      slug: 'thiago-concer-ensina-vendas'
    },
    {
      id: '397652b8-ea9e-4dfd-84aa-0a7cacf16ac8',
      courseGroup: 'Staageclass',
      courseTitle: 'Como criar um podcast do zero',
      courseCenter: 'Lucas Zafra',
      poster: 'https://content.staage.com/_c/397652b8-ea9e-4dfd-84aa-0a7cacf16ac8/_p2.png',
      courseDescription: 'Lucas Zafra',
      slug: 'lucas-zafra-podcast-primocast'
    },
    {
      id: 'c895c0e8-8a6d-4fa8-9141-c09d82c75591',
      courseGroup: 'Staageclass',
      courseTitle: 'Dominando o mercado de lançamentos',
      courseCenter: 'Ellen Salomão',
      poster: 'https://content.staage.com/_c/c895c0e8-8a6d-4fa8-9141-c09d82c75591/_p.png',
      courseDescription: 'Ellen Salomão',
      slug: 'lancamentos-digitais-com-ellen-salomao'
    },
    {
      id: '0b9fefbc-1f37-425f-a212-cb118afe8cbe',
      courseGroup: 'Staageclass',
      courseTitle: 'Como fazer dinheiro no instagram',
      courseCenter: 'Duda Vieira',
      poster: 'https://content.staage.com/_c/0b9fefbc-1f37-425f-a212-cb118afe8cbe/_p.png',
      courseDescription: 'Duda Vieira',
      slug: 'duda-vieira-ensina-instagram'
    }
  ]
}

export default _env
