/* eslint-disable camelcase */
import { extend } from 'vee-validate'
import { required, email, regex, alpha_spaces } from 'vee-validate/dist/rules'

extend('required', {
  ...required,
  message: 'Campo obrigatório'
})

extend('email', {
  ...email,
  message: 'Digite um e-mail válido'
})

extend('phone', {
  ...regex,
  message: 'Digite um telefone válido'
})

extend('onlyLetter', {
  ...alpha_spaces,
  message: 'Digite apenas letras'
})
