export default function ({ $axios }, inject) {
  const lms = $axios.create({
    baseURL: 'https://learning.timeprimo.com/app',
    headers: {
      'Content-Type': 'application/json',
      Service: 'STAAGE'
    }
  })

  const starlink = $axios.create({
    baseURL: 'https://starlink.timeprimo.com/app'
  })

  inject('lms', lms)
  inject('starlink', starlink)
}
