import webpack from 'webpack'

export default {
  ignore: ['pages/old'],
  // mode: 'server',
  // target: 'static',
  generate: {
    fallback: false
  },
  loading: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Staage | Marketing para todo mundo',
    htmlAttrs: {
      lang: 'pt'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { name: 'description', content: 'As principais competências e os bastidores de marketing do mundo ao seu alcance' },
      { property: 'og:locale', content: 'pt_BR' },
      { property: 'og:type', content: 'website' },
      { property: 'og:title', content: 'Staage | Marketing para todo mundo' },
      {
        property: 'og:description',
        content: 'As principais competências e os bastidores de marketing do mundo ao seu alcance'
      },
      { property: 'og:url', content: 'https://staage.com.br/' },
      { property: 'og:site_name', content: 'Staage | Marketing para todo mundo' },
      { property: 'og:image', content: '/favicon-96x96.png' },
      { property: 'og:image:secure_url', content: '/head.png' },
      { property: 'og:image:width', content: '200' },
      { property: 'og:image:height', content: '200' },
      { name: 'twitter:card', content: 'summary' },
      {
        name: 'twitter:description',
        content: 'As principais competências e os bastidores de marketing do mundo ao seu alcance'
      },
      { name: 'twitter:title', content: 'Staage | Marketing para todo mundo' },
      { name: 'twitter:image', content: '/favicon-96x96.png' },
      { name: 'msapplication-TileColor', content: '#F3722C' },
      { name: 'msapplication-TileImage', content: '/ms-icon-144x144.png' },
      { name: 'theme-color', content: '#F3722C' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      { rel: 'icon', type: 'image/png', sizes: '192x192', href: '/android-icon-192x192.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '96x96', href: '/favicon-96x96.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      { re: 'canonical', href: 'https://staage.com.br/' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/klu0six.css' },
      { rel: 'stylesheet', href: 'css/plugins.css' },
      { rel: 'stylesheet', href: 'css/main.css' },
      { rel: 'apple-touch-icon', sizes: '57x57', href: '/apple-icon-57x57.png' },
      { rel: 'apple-touch-icon', sizes: '60x60', href: '/apple-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes: '72x72', href: '/apple-icon-72x72.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: '/apple-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes: '114x114', href: '/apple-icon-114x114.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/apple-icon-120x120.png' },
      { rel: 'apple-touch-icon', sizes: '144x144', href: '/apple-icon-144x144.png' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: '/apple-icon-152x152.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-icon-180x180.png' },
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.15.3/css/all.css',
        integrity: 'sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk',
        crossorigin: 'anonymous'
      },
      { rel: 'manifest', href: '/static/manifest.json' }
    ],
    script: [{ src: 'https://cdn.timeprimo.com/privacy/staage/primoprivacy.min.js' }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/styles/scss/main.scss', 'aos/dist/aos.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/swiper.js',
    '~plugins/vue-prlx.js',
    '~/plugins/xmodal.js',
    '~/plugins/vee-validate.js',
    '~/plugins/vue-the-mask.js',
    '~/plugins/gtm.js',
    '~plugins/axios.js',
    { src: '~/plugins/vue-parallax-js', ssr: false }
  ],

  gtm: {
    enabled: true,
    scriptDefer: true,
    pageTracking: true,
    id: 'GTM-MDH43QJ'
  },
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // '@nuxtjs/eslint-module',
    '@nuxtjs/router-extras'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    'nuxt-clipboard',
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    '@nuxtjs/gtm',
    ['nuxt-clipboard', { autoSetContainer: true }]
  ],
  sitemap: {
    hostname: 'https://staage.com',
    xmlNs: 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"',
    gzip: true,
    exclude: ['/secret']
  },
  robots: {
    UserAgent: '*',
    Allow: '/',
    Sitemap: 'https://staage.com/sitemap.xml'
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // analyze: true,
    extractCSS: true,
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ],
    transpile: ['vee-validate/dist/rules']
  },
  router: {
    routes: [
      {
        name: '/',
        path: '/',
        component: 'pages/index.vue'
      },
      {
        name: 'mentor-slug',
        path: '/mentor/:courseID',
        component: 'pages/mentor.vue'
      },
      {
        name: 'sarahandrade',
        path: '/sarahandrade',
        component: 'pages/sarahandrade.vue'
      },
      {
        name: 'conheca',
        path: '/conheca',
        component: 'pages/conheca.vue'
      },
      // {
      //   name: 'soon',
      //   path: '/soon',
      //   component: 'pages/soon.vue'
      // },
      {
        name: 'webinar',
        path: '/webinar',
        component: 'pages/webinar.vue'
      },
      {
        name: 'p01mktz',
        path: '/p01mktz',
        component: 'pages/p01mktz.vue'
      },
      {
        name: 'obrigado',
        path: '/obrigado',
        component: 'pages/obrigado.vue'
      },
      {
        name: 'redessociaislucrativas',
        path: '/redessociaislucrativas',
        component: 'pages/redessociaislucrativas.vue'
      },
      {
        name: 'obrigadoredes',
        path: '/obrigadoredes',
        component: 'pages/obrigadoredes.vue'
      },
      {
        name: 'content',
        path: '/content',
        component: 'pages/content.vue'
      },
      {
        name: 'l07gt10k',
        path: '/l07gt10k',
        component: 'pages/l07gt10k.vue'
      },
      {
        name: 'l07gt10k_typ',
        path: '/l07gt10k_typ',
        component: 'pages/l07gt10k_typ.vue'
      },
      {
        name: 'p03ap',
        path: '/p03ap',
        component: 'pages/p03ap.vue'
      }
    ]
  }
}
