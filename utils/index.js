export const durationToTime = (duration, showHours = false) => {
  const seconds = parseInt((duration / 1000) % 60)
  const minutes = parseInt((duration / (1000 * 60)) % 60)
  const hours = parseInt((duration / (1000 * 60 * 60)) % 24)
  return (
    (showHours ? `${hours < 10 ? '0' + hours : hours}:` : '') +
    (minutes < 10 ? '0' + minutes : minutes) +
    ':' +
    (seconds < 10 ? '0' + seconds : seconds)
  )
}

export const initAccordion = (cardClass, cardId) => {
  const accordionList = document.querySelectorAll(cardClass)
  const accordion = document.getElementById(cardId)

  accordionList.forEach((item) => {
    if (item.id !== cardId) {
      item.classList.remove('active')
    }
  })

  accordion.classList.contains('active') ? accordion.classList.remove('active') : accordion.classList.add('active')
}

export const redirectToMentor = (courseID) => {
  window.location.pathname = `/mentor/${courseID}/${window.location.search}`
}
