export const actions = {
  async search(_, search) {
    try {
      return await this.$starlink.$post('/user/search', search)
    } catch (error) {
      console.error(error)
    }
  },
  async campaing(_, campaing) {
    if (campaing.eventMeta && typeof campaing.eventMeta !== 'string') {
      campaing.eventMeta.url = campaing.eventMeta?.url?.split('#')[0]
    }
    try {
      return await this.$starlink.$post('/conversion/campaign', campaing)
    } catch (err) {
      console.error(err)
    }
  }
}
