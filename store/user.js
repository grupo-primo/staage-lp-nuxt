export const state = () => ({
  showExitOverlayPrincipal: null,
  showExitOverlayChristmas: null
})

export const mutations = {
  setShowExitOverlayPrincipal(state, showExitOverlayPrincipal) {
    state.showExitOverlayPrincipal = showExitOverlayPrincipal
  },
  setShowExitOverlayChristmas(state, showExitOverlayChristmas) {
    state.showExitOverlayChristmas = showExitOverlayChristmas
  }
}
