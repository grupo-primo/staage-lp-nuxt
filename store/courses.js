export const actions = {
  async getCourses(_) {
    try {
      const response = await this.$lms.get('/courses', { perPage: 100, page: 1 })
      return response.data.data
    } catch (err) {
      console.error(err)
    }
  },
  async getCourseByID(_, courseID) {
    try {
      const response = await this.$lms.get(`/public/courses/${courseID}`)
      return response.data.data
    } catch (err) {
      console.error(err)
    }
  }
}
