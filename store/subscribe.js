export const actions = {
  async makeLeadSubscription(_, data) {
    try {
      return await this.$axios.$post('https://lm.timeprimo.com/app/lead/subscribe', data)
    } catch (error) {
      console.error(error)
    }
  }
}
